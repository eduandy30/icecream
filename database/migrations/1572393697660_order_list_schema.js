'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrderListSchema extends Schema {
  up () {
    this.create('order_lists', (table) => {
      table.increments()
      table.string('name')
      table.datetime('expiration_date')
      table.integer('person_in_charge').unsigned().references('id').inTable('users').onDelete('cascade')
      table.integer('user_id').unsigned().references('id').inTable('users').onDelete('cascade')
      table.timestamps()
    })
  }

  down () {
    this.drop('order_lists')
  }
}

module.exports = OrderListSchema
