'use strict'

const OrderRepo = use('OrderRepository')
const BaseController = use('BaseController')
const Order = use('App/Models/Order')
const OrderList = use('App/Models/OrderList')
const _ = require('lodash')
const Mail = use('Mail')

class OrderListController extends BaseController{

  constructor(){
    super(OrderRepo)
    this.repo = OrderRepo
  }

  async addOrderList({request, auth, response}){

    try{
      let order = await Order.query().where('user_id', request.input('user_id')).where('order_list_id', request.input('order_list_id')).fetch()
      let order_list = await OrderList.find(request.input('order_list_id'))

      if(this.isOrderListActive(order_list)){
        if(_.size(order.rows) == 0){
          let order = await Order.create(request.all())
          return response.json({ message: 'Order created successfully', paylaod: order })
        }else{
          return response.json({message: 'This user has already an order'})
        }
      }else{
        return response.json({ message: 'This order list is no longer active' })
      }

    }catch(e){
      return response.json({message: 'Unexpected error'})
    }

  }

  async removeOrderList({request, auth, response}){
    try{
      let order = await Order.query().where('user_id', request.input('user_id')).fetch()
      order.delete()

      return response.json({ message: 'Order successfully deleted!' })

    }catch(e){
      return response.json({ message: 'Unexpected error!' })
    }
  }

  async isOrderListActive(order_list){
    if(!order_list)
      return false

    let order_date = new Date(order_list.expiration_date)
    let current_date = new Date()

    if(current_date >= order_date)
      return false
    else
      return true
    
  }

  async sendMail(user_id){
    let user = await user.find(user_id)
    await Mail.send('emails.icecream', {user}, (message) => {
      message.from('foo@bar.com')
      message.to(user.email)
    })
  }

}

module.exports = OrderListController