'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrderSchema extends Schema {
  up () {
    this.create('orders', (table) => {
      table.increments()
      table.string('description')
      table.decimal('price')
      table.integer('user_id').unsigned().references('id').inTable('users').onDelete('cascade')
      table.integer('order_list_id').unsigned().references('id').inTable('order_lists').onDelete('cascade')
      table.string('payment_method')
      table.boolean('payed')
      table.timestamps()
    })
  }

  down () {
    this.drop('orders')
  }
}

module.exports = OrderSchema
