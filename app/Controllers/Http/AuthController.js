'use strict'

const User = use('App/Models/User')

class AuthController {

  async login({request, auth, response}) {
    const username = request.input("username")
    const password = request.input("password");
    try {
      if (await auth.attempt(username, password)) {
        let user = await User.findBy('username', username)
        let accessToken = await auth.generate(user)
        return response.json({"user":user, "access_token": accessToken})
      }

    }
    catch (e) {
      return response.json({message: 'Wrong credentials!'})
    }
  }

}

module.exports = AuthController
