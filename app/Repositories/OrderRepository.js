'use strict'

const { ioc } = require('@adonisjs/fold')
const BaseRepository = use('App/Repositories/BaseRepository')

class OrderRepository extends BaseRepository{

  constructor(model){
    super(model)
    this.model = model
  }

}

ioc.singleton('OrderRepository', function (app) {
  const Model = app.use('App/Models/OrderList')
  return new OrderRepository(Model)
})

module.exports = ioc.use('OrderRepository')

