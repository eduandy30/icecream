'use strict'

const _ = require('lodash')

const User = use('App/Models/User')
const Order = use('App/Models/OrderList')

class BaseRepository{

  constructor(model){
    this.model = model
    this.noRecordFound = 'No record found'
  }

  async index(response){
    let result = await this.model.all()
    return response.json({data:result})
  }


  async store(request, response, auth){
    if(this.model.name == 'User'){
      return this.saveUser(request, response)
    }else{
      return this.saveOrder(request, auth, response)
    }

  }


  async show(params,response){
    const modelObj = await this.model.find(params.id)
    if(!modelObj){
      return response.status(404).json({message:this.noRecordFound})
    }
    return response.json({data:modelObj})
  }


  async update(params,request,response){
    const input = request.all()
    const modelObj = await this.model.find(params.id)

    //check if the row related to this id exists
    if(!modelObj){
      return response.status(404).json({message:this.noRecordFound})
    }

    /*
    *check if the input is not empty -> No need to check here, validator on route will take care of it
    */

    //assigning input data in db fields
    _.forEach(input,function(e,i){
      modelObj[i] = e
    })

    await modelObj.save()
    return response.status(200).json({message: this.model.name + ' has been updated', data:modelObj})
  }


  async destroy(params,response){
    const modelObj = await this.model.find(params.id)
    if(!modelObj){
      return response.status(404).json({data:this.noRecordFound})
    }
    await modelObj.delete()
    return response.status(200).json({message:this.model.name+ " deleted", data: modelObj})
  }

  async saveUser(request, response){
    
    let input = request.except(['password_confirmation']);
    let modelObj = new this.model()


    _.forEach(input,function(e,i){
      modelObj[i] = e
    })

    await modelObj.save()

    return response.status(201).json({ message: this.model.name + ' created successfully', data: modelObj })
  }

  async saveOrder(request, auth, response){

    let order = new this.model()

    try {

      order.user_id = auth.user.id
      order.expiration_date = request.input('expiration_date')
      order.name = request.input('name')
      order.person_in_charge = request.input('person_in_charge')
      await order.save()
  
      return response.status(201).json({ message: this.model.name + ' created successfully', data: order })

    }catch(e){
      return response.json({message: 'Unexpected error'})
    }
  }
}

module.exports = BaseRepository

